# twitter实时爬虫



## 项目结构

* config.json：configuration文件
* main.py：程序入口，负责和用户交互
* slistener.py：与twitter api交互，抓取数据
* twitterClient.py：处理抓取的数据
* MysqlHandler.py: 处理与数据库有关操作，包括创建数据库，表，插入数据等
* utils.py：辅助功能
* users.csv：要抓取的用户id




## 设置说明

* config.json

  设置consumer_key, consumer_secret, access_token, access_token_secret四个字段，使用文件中的默认值即可，此文件不需要更改

* users.csv

  用于爬取特定用户的推文，每行为一个用户id（用户id是一个64位 unsigned integers）

* slistener.py

  设置数据库database名字及table名字




## 使用说明

#### 命令行接受两个参数：

* -c, --config: 设置configration文件的路径，默认为"./config.json"
* -u, --users: 设置users文件的路径，默认为"./users.csv"

以下用法是等价的，若不改变文件默认位置，只需编辑对应要抓取内容的文件，将另一个文件设为空文件，使用第一种命令即可：

```bash
python main.py
python main.py -c ./config.json
python main.py -c ./config.json -u ./users.csv
```



## 结果说明

可以爬取的数据包括：

* 要爬取的用户列表中的用户的发推、转推、回复操作
* 所有用户（不论是否在users.csv中）对要爬取的用户的回复，并可获取回复内容、回复的目标用户和目标推文
* 所有用户（不论是否在users.csv中）对要爬取的用户的转发，并可获取转发的内容，被转发的用户的用户名需要从转发内容中自行提取，若转发的同时发表评论，不会抓取到该评论
* 所有用户（不论是否在users.csv中）对要爬取的用户转发内容的再次转发，但只能获取到转发内容和原始发推人的名字

在数据库中每条数据包括9个字段：

* tweet_id: 该条tweet的id值，每条tweet的值均不同

* user_id: 发推用户的id值

* user_name: 发推用户的昵称

* user_screen_name: 发推用户的screen_name

  （user_name指用户名中"@"号前面的内容，screen_name指用户名中“@”号后面的内容，如对于用户名"Serena Williams@serenawilliams"，user_name值为"Serena Williams"，screen_name值为"serenawilliams"，在这里存储user_name的主要作用是便于人读数据，程序用user_id来标识用户更易于处理；存储screen_name的主要作用是，若有爬取列表之外的用户转发了爬取列表中用户的tweet，我们能爬取到的唯一有关被转发用户的信息就是推文开头的"RT @user_screen_name"中的user_screen_name）

* tweet: 推文内容

* post_time: 发推时间

* status: 该字段可取三个值："original", "reply"或"retweet"，分别标识该条tweet是原推、回复还是转发

* reply_to_user_id: 若为原创推文，该字段值为"None"；若为对其他推文的回复，该字段值为回复对象推文的user_id值

* reply_to_tweet_id: 若为原创推文，该字段值为"None"；若为对其他推文的回复，该字段值为回复对象推文的tweet_id值

* retweet_from_screen_name: 若不是转发的推文，该字段值为"None"；若为对其他推文的转发，该字段值为被转发的推文的作者的user_screen_name






## 附

JSON representation of a status object:

```json
[
  {
    "coordinates": null,
    "truncated": false,
    "created_at": "Thu Oct 14 22:20:15 +0000 2010",
    "favorited": false,
    "entities": {
      "urls": [
      ],
      "hashtags": [
      ],
      "user_mentions": [
        {
          "name": "Matt Harris",
          "id": 777925,
          "id_str": "777925",
          "indices": [
            0,
            14
          ],
          "screen_name": "themattharris"
        }
      ]
    },
    "text": "@themattharris hey how are things?",
    "annotations": null,
    "contributors": [
      {
        "id": 819797,
        "id_str": "819797",
        "screen_name": "episod"
      }
    ],
    "id": 12738165059,
    "id_str": "12738165059",
    "retweet_count": 0,
    "geo": null,
    "retweeted": false,
    "in_reply_to_user_id": 777925,
    "in_reply_to_user_id_str": "777925",
    "in_reply_to_screen_name": "themattharris",
    "user": {
      "id": 6253282,
      "id_str": "6253282"
    },
    "source": "web",
    "place": null,
    "in_reply_to_status_id": 12738040524,
    "in_reply_to_status_id_str": "12738040524"
  }
]
```







