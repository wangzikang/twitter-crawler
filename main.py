#!/usr/bin/env python3

import argparse
from twitterClient import twitterClient
from utils import csv_to_list


def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("-c", "--config", default="./config.json", help="path of the config file.")
    ## parser.add_argument("-t", "--topic", default="./topic.csv", help="filter streaming tweets by topic, the path of file that contains topics.")
    parser.add_argument("-u", "--users", default="./users.csv", help="filter streaming tweets by users, the path of file that contains users.")

    args = parser.parse_args()

    config_path = str(args.config)
    ## topic_path = str(args.topic)
    users_path = str(args.users)

    ## track = csv_to_list(topic_path)
    users = csv_to_list(users_path)

    client = twitterClient(config_path)
    '''
    if track:
        client.get_topic_tweets(track)
    if users:
        client.get_user_tweets(users)
    if (not track and not users):
        print("Please specify the filter list!")
    if track and users:
        print("Twitter only allows one streaming collector! Please choose one from topic and users.")
    '''
    if users:
        client.get_user_tweets(users)
    if not users:
        print("The user id file is empty!")

if __name__ == '__main__':
    main()
