from __future__ import unicode_literals
import re
import tweepy
import json
import traceback
from tweepy import OAuthHandler
from slistener import SListener


class twitterClient(object):
    def __init__(self, config_path):

        #config_data = json.load(config_path)
        with open(config_path) as jsonfile:
            config_data = json.load(jsonfile)
        consumer_key = config_data['consumer_key']
        consumer_secret = config_data['consumer_secret']
        access_token = config_data['access_token']
        access_token_secret = config_data['access_token_secret']

        try:
            self.auth = OAuthHandler(consumer_key, consumer_secret)
            self.auth.set_access_token(access_token, access_token_secret)
            self.api = tweepy.API(self.auth)
        except:
            print("Error: Authentication Failed")

    '''
    def get_topic_tweets(self, track):

        listener = SListener(self.api, 'topic')
        stream = tweepy.Stream(self.auth, listener)
        print('Start crawling twitter by topic...')

        try:
            stream.filter(track = track)
        except Exception as e:
            # traceback.print_exc()
            print('error: ', str(e))
            stream.disconnect()
    '''

    def get_user_tweets(self, users):

        listener = SListener(self.api)
        stream = tweepy.Stream(self.auth, listener)
        print('Start crawling twitter by users...')

        try:
            stream.filter(follow = users)
        except Exception as e:
            traceback.print_exc()
            print('error: ', str(e))
            stream.disconnect()


