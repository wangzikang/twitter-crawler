import MySQLdb

class MysqlHandler:

    def __init__(self, host = "localhost", user = "root", passwd = "root", db = "Twitter"):
        self.host = host
        self.user = user
        self.passwd = passwd
        self.db = db
        self.connection = MySQLdb.connect(host=self.host, user=self.user, passwd=self.passwd)
        self.cursor = self.connection.cursor()
        
        self.cursor.execute('CREATE DATABASE IF NOT EXISTS {}'.format(self.db))
        self.connection.select_db(self.db)

    def create_table(self, table_name):
        create_sql = """CREATE TABLE IF NOT EXISTS {}
                        (tweet_id varchar(128), 
                        user_id varchar(128), 
                        user_name varchar(128), 
                        user_screen_name varchar(128),
                        tweet varchar(2000), 
                        post_time varchar(50), 
                        status varchar(20),
                        reply_to_user_id varchar(128), 
                        reply_to_tweet_id varchar(128),
                        retweet_from_screen_name varchar(128))""".format(table_name)
        self.cursor.execute(create_sql)

        self.connection.set_character_set('utf8')
        self.cursor.execute('SET NAMES utf8;')
        self.cursor.execute('SET CHARACTER SET utf8;')
        self.cursor.execute('SET character_set_connection=utf8;')
        self.cursor.execute('alter table {} convert to character set utf8;'.format(table_name))

    def insert_data(self, table_name, data):
        insert_sql = "INSERT INTO {} VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)".format(table_name)
        self.cursor.execute(insert_sql, data)
        self.connection.commit()

