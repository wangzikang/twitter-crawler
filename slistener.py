from tweepy import StreamListener
import json, time, sys
from MysqlHandler import MysqlHandler
import re

class SListener(StreamListener):

    def __init__(self, api = None):
        self.api = api or API()
        self.counter = 0
        # self.delout  = open('./data/delete.txt', 'a')
        # set database name, default is "twitter"
        self.mysql = MysqlHandler()
        # set table name
        self.table_name = "users"

        self.mysql.create_table(self.table_name)
        
    def on_data(self, data):

        if  'in_reply_to_status' in data:
            self.on_status(data)
        elif 'delete' in data:
            pass
            '''
            delete = json.loads(data)['delete']['status']
            if self.on_delete(delete['id'], delete['user_id']) is False:
                return False
            '''
        elif 'limit' in data:
            if self.on_limit(json.loads(data)['limit']['track']) is False:
                return False
        elif 'warning' in data:
            warning = json.loads(data)['warnings']
            print(warning['message'])
            return false

    def on_status(self, status):
        data = json.loads(status)
        tweet_id = data['id_str']
        user_id = data['user']['id_str']
        user_name = data['user']['name']
        user_screen_name = data['user']['screen_name']
        tweet = data['text']
        time = data['created_at']
        reply_to_user_id = str(data['in_reply_to_user_id_str'])
        reply_to_tweet_id = str(data['in_reply_to_status_id_str'])


        pattern = re.compile(r'^RT\s@(.*?):', re.DOTALL)
        match = pattern.match(tweet)
        if match:
            retweet_from_screen_name = match.group()[4:-1]
        else:
            retweet_from_screen_name = "None"
        
        if reply_to_user_id != "None":
            status = "reply"
        elif retweet_from_screen_name != "None":
            status = "retweet"
        else:
            status = "original"

        data_list = (tweet_id, user_id, user_name,
                     user_screen_name, tweet, time,
                     status, reply_to_user_id, 
                     reply_to_tweet_id,
                     retweet_from_screen_name)

        self.mysql.insert_data(self.table_name, data_list)

        self.counter += 1

        print("writing...id:{}, count:{}".format(tweet_id, self.counter))

        return

    def on_delete(self, status_id, user_id):
        '''
        self.delout.write( str(status_id) + "\n")
        return
        '''
        pass

    def on_limit(self, track):
        sys.stderr.write(str(track) + "\n")
        return

    def on_error(self, status_code):
        sys.stderr.write('Error: ' + str(status_code) + "\n")
        return False

    def on_timeout(self):
        sys.stderr.write("Timeout, sleeping for 60 seconds...\n")
        time.sleep(60)
        return

