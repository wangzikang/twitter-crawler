import csv
from numpy import array

# 每行整体看作一个关键字
# TODO: 每行中存在逗号时会出问题
def csv_to_list(path):
    l = []
    reader = csv.reader(open(path))
    for row in reader:
        l.append(row)

    return array(l).flatten().tolist()


